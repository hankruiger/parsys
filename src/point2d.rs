use std::ops;

#[derive(Copy, Clone, Debug)]
pub struct Point2D(pub f32, pub f32);

impl Point2D {
  // Return the Euclidean norm of the vector
  pub fn norm(&self) -> f32 {
    (self.0 * self.0 + self.1 * self.1).sqrt()
  }

  // Normalize the vector in-place (and also return it to keep it ergonomic)
  pub fn normalize(mut self) -> Self {
    let norm = self.norm();
    self /= norm;
    self
  }
}

impl ops::Add<Point2D> for Point2D {
  type Output = Point2D;

  fn add(self, rhs: Point2D) -> Point2D {
      Point2D(self.0 + rhs.0, self.1 + rhs.1)
  }
}

impl ops::AddAssign<Point2D> for Point2D {
  fn add_assign(&mut self, rhs: Point2D) {
    self.0 += rhs.0;
    self.1 += rhs.1;
  }
}

impl ops::Sub<Point2D> for Point2D {
  type Output = Point2D;

  fn sub(self, rhs: Point2D) -> Point2D {
      Point2D(self.0 - rhs.0, self.1 - rhs.1)
  }
}

impl ops::SubAssign<Point2D> for Point2D {
  fn sub_assign(&mut self, rhs: Point2D) {
    self.0 -= rhs.0;
    self.1 -= rhs.1;
  }
}

impl ops::Mul<f32> for Point2D {
  type Output = Point2D;

  fn mul(self, rhs: f32) -> Point2D {
      Point2D(self.0 * rhs, self.1 * rhs)
  }
}

impl ops::MulAssign<f32> for Point2D {
  fn mul_assign(&mut self, rhs: f32) {
    self.0 *= rhs;
    self.1 *= rhs;
  }
}

impl ops::Div<f32> for Point2D {
  type Output = Point2D;

  fn div(self, rhs: f32) -> Point2D {
      Point2D(self.0 / rhs, self.1 / rhs)
  }
}

impl ops::DivAssign<f32> for Point2D {
  fn div_assign(&mut self, rhs: f32) {
    self.0 /= rhs;
    self.1 /= rhs;
  }
}