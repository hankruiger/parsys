use crate::point2d::*;
use crate::physics::*;

pub type NodeId = u32;

#[derive(Copy, Clone)]
pub struct Node {
  pub id: NodeId,
  pub physics: Physics
}

impl Node {
  pub fn new(id: NodeId, center: Point2D, radius: f32) -> Self {
    Node {
      id,
      physics : Physics {
        geometry: DiscGeometry {
          center, radius
        },
        mass: 1.0,
        velocity: Some(Point2D(0.0, 0.0))
      }
    }
  }
}
