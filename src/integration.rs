use std::collections::HashMap;

use crate::{point2d::Point2D, ParSys};

pub enum IntegrationMethod {
  ExplicitEuler,
  RungeKutta4
}

pub trait ExplicitEuler {
  fn step_explicit_euler(&mut self);
}

impl ExplicitEuler for ParSys {
  fn step_explicit_euler(&mut self) {
    let xs = self.nodes.iter().map(|(n_id, n)| {
        (*n_id, n.physics.geometry.center)
      }).collect();
    let vs = self.nodes.iter().map(|(n_id, n)| {
      (*n_id, match n.physics.velocity {
        Some(v) => v,
        None => Point2D(0.0, 0.0)
      })
    }).collect();
    let accs = self.compute_acc(&xs, &vs);

    for (n_id, n) in self.nodes.iter_mut() {
      if let Some(v) = &mut n.physics.velocity {
        n.physics.geometry.center += *vs.get(n_id).unwrap() * self.timestep;
        *v += *accs.get(n_id).unwrap() * self.timestep;
      }
    }
  }
}

pub trait RungeKutta4 {
  fn step_rk4(&mut self);
}

impl RungeKutta4 for ParSys {
  fn step_rk4(&mut self) {
    let xs_t = self.nodes.iter().map(|(n_id, n)| {
      (*n_id, n.physics.geometry.center)
    }).collect();
    let vs_t = self.nodes.iter().map(|(n_id, n)| {
      (*n_id, match n.physics.velocity {
        Some(v) => v,
        None => Point2D(0.0, 0.0)
      })
    }).collect();
  
    let accs_t = self.compute_acc(&xs_t, &vs_t);
    
    let xs_mid_1: HashMap<u32, Point2D> = self.nodes.iter().map(|(n_id, _)| {
      let x_t = *xs_t.get(n_id).unwrap();
      let v_t = *vs_t.get(n_id).unwrap();
      (*n_id, x_t + v_t * (self.timestep / 2.0))
    }).collect();
    let vs_mid_1: HashMap<u32, Point2D> = self.nodes.iter().map(|(n_id, _)| {
      let v_t = *vs_t.get(n_id).unwrap();
      let acc_t = *accs_t.get(n_id).unwrap();
      (*n_id, v_t + acc_t * (self.timestep / 2.0))
    }).collect();
  
    let accs_mid_1 = self.compute_acc(&xs_mid_1, &vs_mid_1);

    let xs_mid_2: HashMap<u32, Point2D> = self.nodes.iter().map(|(n_id, _)| {
      let x_t = *xs_t.get(n_id).unwrap();
      let v_mid_1 = *vs_mid_1.get(n_id).unwrap();
      (*n_id, x_t + v_mid_1 * (self.timestep / 2.0))
    }).collect();
    let vs_mid_2: HashMap<u32, Point2D> = self.nodes.iter().map(|(n_id, _)| {
      let v_t = *vs_t.get(n_id).unwrap();
      let acc_mid_1 = *accs_mid_1.get(n_id).unwrap();
      (*n_id, v_t + acc_mid_1 * (self.timestep / 2.0))
    }).collect();

    let accs_mid_2 = self.compute_acc(&xs_mid_2, &vs_mid_2);
    
    let xs_end: HashMap<u32, Point2D> = self.nodes.iter().map(|(n_id, _)| {
      let x_t = *xs_t.get(n_id).unwrap();
      let v_mid_2 = *vs_mid_2.get(n_id).unwrap();
      (*n_id, x_t + v_mid_2 * self.timestep)
    }).collect();
    let vs_end: HashMap<u32, Point2D> = self.nodes.iter().map(|(n_id, _)| {
      let v_t = *vs_t.get(n_id).unwrap();
      let acc_mid_2 = *accs_mid_2.get(n_id).unwrap();
      (*n_id, v_t + acc_mid_2 * self.timestep)
    }).collect();

    let accs_end = self.compute_acc(&xs_end, &vs_end);

    for (n_id, n) in self.nodes.iter_mut() {
      if let Some(v) = &mut n.physics.velocity {
        let x_t = *xs_t.get(n_id).unwrap();
        
        let v_t = *vs_t.get(n_id).unwrap();
        let v_mid_1 = *vs_mid_1.get(n_id).unwrap();
        let v_mid_2 = *vs_mid_2.get(n_id).unwrap();
        let v_end = *vs_end.get(n_id).unwrap();
        
        let acc_t = *accs_t.get(n_id).unwrap();
        let acc_mid_1 = *accs_mid_1.get(n_id).unwrap();
        let acc_mid_2 = *accs_mid_2.get(n_id).unwrap();
        let acc_end = *accs_end.get(n_id).unwrap();
        
        n.physics.geometry.center = x_t + (v_t + v_mid_1 * 2.0 + v_mid_2 * 2.0 + v_end) * (self.timestep / 6.0);
        *v = v_t + (acc_t + acc_mid_1 * 2.0 + acc_mid_2 * 2.0 + acc_end) * (self.timestep / 6.0);
      }
    }
  }
}
