use crate::Node;
use crate::Point2D;

#[derive(Copy, Clone)]
pub struct Physics {
  pub geometry: DiscGeometry,
  pub mass: f32,
  pub velocity: Option<Point2D>,
}

#[derive(Copy, Clone)]
pub struct DiscGeometry {
  pub center: Point2D,
  pub radius: f32,
}

// This type name is a bit clumsy and the signature could be improved...
pub type FnThatComputesForceFromNode = fn(
  // The (approximated while integrating) positions and velocities of both nodes
  x_v_1: (&Point2D, &Point2D),
  x_v_2: (&Point2D, &Point2D),

  // Reference to the nodes themselves to get other properties. (Positions and
  // velocities can be outdated during integration so don't use them!)
  n1: &Node, n2: &Node
) -> Point2D;

pub struct Force {
  pub selector: fn(&Node, &Point2D, &Point2D) -> bool,
  pub force_from_node: FnThatComputesForceFromNode
  // TODO: Currently the candidates for nodes where this node receives forces
  // from is *all* nodes, but it can be made more efficient by restricting it
  // to a smaller set of candidates.
}
