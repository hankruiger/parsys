# `parsys`
This is an experimental `par`ticle `sys`tem simulator.
The API is very likely to change so using it in your project is currently not recommended.

## Integration step
Two integration methods are implemented: Explicit Euler (EE) and Runge-Kutta 4 (RK4).
EE is faster but less accurate.

### Explicit Euler (EE)

For a simulation step from $`t`$ to $`t + h`$, we need as input:

- $`\vec{x}^t`$: Positions of all particles at time $`t`$.
- $`\vec{v}^t`$: Velocities of all particles at time $`t`$.
- $`\vec{a}(\vec{x}, \vec{v})`$: A function that, given all positions and velocities, computes the accelerations.

As output, we will compute approximations of $`\vec{x}^{t + h}`$ and $`\vec{v}^{t + h}`$.

Explicit Euler integration works as follows:

- Compute $`\vec{a}^t = \vec{a}(\vec{x}^t, \vec{v}^t)`$
- Approximate (discretization error $`O(h^2)`$):
  - $`\vec{x}^{t + h} \approx \vec{x}^t + h \vec{v}^t`$
  - $`\vec{v}^{t + h} \approx \vec{v}^t + h \vec{a}^t`$

### Runge-Kutta (RK4)

For a simulation step from $`t`$ to $`t + h`$, we need as input:

- $`\vec{x}^t`$: Positions of all particles at time $`t`$.
- $`\vec{v}^t`$: Velocities of all particles at time $`t`$.
- $`\vec{a}(\vec{x}, \vec{v})`$: A function that, given all positions and velocities, computes the accelerations.

As output, we will compute approximations of $`\vec{x}^{t + h}`$ and $`\vec{v}^{t + h}`$.

RK4 integration works as follows:

- Compute $`\vec{a}^t = \vec{a}(\vec{x}^t, \vec{v}^t)`$
- Compute $`\vec{x}^{mid_1} = \vec{x}^t + \frac{h}{2}\vec{v}^t`$ and $`\vec{v}^{mid_1} = \vec{v}^t + \frac{h}{2}\vec{a}^t`$
- Compute $`\vec{a}^{mid_1} = \vec{a}(\vec{x}^{mid_1}, \vec{v}^{mid_1})`$
- Compute $`\vec{x}^{mid_2} = \vec{x}^t + \frac{h}{2}\vec{v}^{mid_1}`$ and $`\vec{v}^{mid_2} = \vec{v}^t + \frac{h}{2}\vec{a}^{mid_1}`$
- Compute $`\vec{a}^{mid_2} = \vec{a}(\vec{x}^{mid_2}, \vec{v}^{mid_2})`$
- Compute $`\vec{x}^{end} = \vec{x}^t + h\vec{v}^{mid_2}`$ and $`\vec{v}^{end} = \vec{v}^t + h\vec{a}^{mid_2}`$
- Compute $`\vec{a}^{end} = \vec{a}(\vec{x}^{end}, \vec{v}^{end})`$
- Approximate (discretization error $`O(h^5)`$):
  - $`\vec{x}^{t + h} \approx \vec{x}^t + h \frac{\vec{v}^t + 2 \vec{v}^{mid_1} + 2 \vec{v}^{mid_2} + \vec{v}^{end}}{6}`$
  - $`\vec{v}^{t + h} \approx \vec{v}^t + h \frac{\vec{a}^t + 2 \vec{a}^{mid_1} + 2 \vec{a}^{mid_2} + \vec{a}^{end}}{6}`$