use gnuplot::{Caption, PlotOption::Color};
use gnuplot::Graph;
use gnuplot::AxesCommon;
use gnuplot::Figure;
use clap::Clap;
use parsys::{FnThatComputesForceFromNode, IntegrationMethod, ParSys, Point2D};

#[derive(Clap)]
#[clap(name = "parsys-example-moon", version = "0.1", author = "Han Kruiger")]
struct Opts {
  renderer: String,
  output: String,
}

fn main() {
  let opts = Opts::parse();

  let (_time, position_ee) = simulate_with(0.01, IntegrationMethod::ExplicitEuler, 50.0);
  let (_time, position_rk) = simulate_with(0.01, IntegrationMethod::RungeKutta4, 50.0);

  let mut fg = Figure::new();
  fg.set_terminal(&opts.renderer, &opts.output);
  fg.axes2d()
    .set_title("Trajectory", &[])
    .set_legend(Graph(0.3), Graph(1.0), &[], &[])
    .set_x_label("x", &[])
    .set_y_label("y", &[])
    .lines(
      &position_ee.iter().map(|p| p.0).collect::<Vec<f32>>(),
      &position_ee.iter().map(|p| p.1).collect::<Vec<f32>>(),
      &[Caption("EE (h=.01)"), Color("red")],
    )
    .lines(
      &position_rk.iter().map(|p| p.0).collect::<Vec<f32>>(),
      &position_rk.iter().map(|p| p.1).collect::<Vec<f32>>(),
      &[Caption("RK4 (h=.01)"), Color("blue")],
    );
  fg.show().unwrap();
}

fn simulate_with(timestep: f32, method: IntegrationMethod, until: f32) -> (Vec<f32>, Vec<Point2D>) {
  let mut system = ParSys::new(timestep, method);

  system.add_static_node(Point2D(0.0, 0.0), 0.0);
  let n_id = system.add_node_with_velocity(Point2D(1.0, 0.0), 0.0, Point2D(0.0, 1.0));

  let gravity: FnThatComputesForceFromNode = |(pos1, _v1), (pos2, _v2), n, m| {
    // Don't try to apply this force to oneself.
    if n.id == m.id {
      return Point2D(0.0, 0.0);
    }

    // Vector from n to m
    let delta = *pos2 - *pos1;
    delta / delta.norm().powi(3)
  };

  system.add_force(|_, _, _| true, gravity);

  let mut times = vec![];
  let mut positions = vec![];

  let mut t = 0.0;
  while t < until {
    times.push(t);
    positions.push(*system.pos(n_id).unwrap());
    system.step();
    t += system.timestep;
  }
  times.push(t);
  positions.push(*system.pos(n_id).unwrap());

  (times, positions)
}
