use gnuplot::{Caption, PlotOption::Color};
use gnuplot::Graph;
use gnuplot::AxesCommon;
use gnuplot::Figure;
use clap::Clap;
use parsys::{FnThatComputesForceFromNode, IntegrationMethod, ParSys, Point2D};

#[derive(Clap)]
#[clap(name = "parsys-example-harmonic-oscillator",
  version = "0.1", author = "Han Kruiger")]
struct Opts {
  renderer: String,
  output: String,
}

fn main() {
  let opts = Opts::parse();

  let (times_ee, distances_ee) = simulate_with(0.01, IntegrationMethod::ExplicitEuler, 60.0);
  let (times_rk4, distances_rk4) = simulate_with(0.5, IntegrationMethod::RungeKutta4, 60.0);

  let cos: Vec<f32> = times_ee.iter().map(|t| t.cos()).collect();

  let mut fg = Figure::new();
  fg.set_terminal(&opts.renderer, &opts.output);
  fg.axes2d()
    .set_title("Displacement vs time", &[])
    .set_legend(Graph(0.3), Graph(1.0), &[], &[])
    .set_x_label("time", &[])
    .set_y_label("displacement", &[])
    .lines(
      &times_ee,
      &distances_ee,
      &[Caption("EE (h=0.01)"), Color("red")],
    )
    .lines(
      &times_rk4,
      &distances_rk4,
      &[Caption("RK4 (h=0.5)"), Color("blue")],
    )
    .lines(
      &times_ee,
      &cos,
      &[Caption("cos(t) (exact)"), Color("#228B22")],
    );
  fg.show().unwrap();
}

fn simulate_with(timestep: f32, method: IntegrationMethod, until: f32) -> (Vec<f32>, Vec<f32>) {
  let mut system = ParSys::new(timestep, method);

  let n_id = system.add_node(Point2D(1.0, 0.0), 1.0);
  system.add_static_node(Point2D(0.0, 0.0), 1.0);

  let spring_force: FnThatComputesForceFromNode = |(pos1, _v1), (pos2, _v2), n, m| {
    // Don't try to apply this force to oneself.
    if n.id == m.id {
      return Point2D(0.0, 0.0);
    }

    // Vector from n to m
    let delta = *pos2 - *pos1;
    // Return that same vector (spring constant 1)
    delta
  };

  system.add_force(|_, _, _| true, spring_force);

  let mut times = vec![];
  let mut displacements = vec![];

  let mut t = 0.0;
  while t < until {
    times.push(t);
    displacements.push(system.pos(n_id).unwrap().0);
    system.step();
    t += system.timestep;
  }
  times.push(t);
  displacements.push(system.pos(n_id).unwrap().0);

  (times, displacements)
}
