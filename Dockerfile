FROM alpine:edge

# Install necessary packages.
RUN apk add --no-cache cargo gnuplot ttf-opensans

# Update the font cache.
RUN fc-cache -fv
